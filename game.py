from random import randint

name = input("Hi! What's your name? ")


for guess_number in range(1,6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print("Guess " + str(guess_number) + ":" + name + ", were you born in " + str(month_number) + "/" + str(year_number) + "?")

    response = input("yes or no?")

    if response == "yes":
            print("I knew it!")
            exit()
    elif guess_number < 5:
            print("Drat! Lemme try again!")
    else:
            print("I have other things to do. Good bye.")
